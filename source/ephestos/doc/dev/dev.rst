==========
Dev Manual
==========

############
Introduction
############

Welcome to the Ephestos Python API reference documentation 

.. _hecate-api-label:


.. include:: hpy/core.rst

########
Aura API
########

.. automodule:: ephpy.apy

################
pylivecoding API
################

.. automodule:: ephpy.pylivecoding

.. _random_notes:

.. include:: notes/random_notes.rst

