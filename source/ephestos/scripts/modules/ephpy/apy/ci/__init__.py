"ACI module , Aura Component Interface"
import aci

class Component:
    def __init__(self,name="none",path="/core/"):
        self.name = name
        self.path = path
        self.is_loaded = False

    def load(self):
        
        if self.name != "none":
            aci.load(self.name,self.path)
            self.is_loaded = True
        else:
            raise ValueError("you need to set the name first")
            return "error"

    def unload(self):
        if self.name != "none":
            if self.is_loaded:
                aci.unload(self.name)
                self.is_loaded = False
            else:
                raise ValueError("you need to load a component first to be able to unload it")
                return "error"
        else:
            raise ValueError("you need to set the name first")
            return "error"

    def request(self,name_of_function,arguments_list):
        if self.is_loaded:
            aci.request(name_of_function,arguments_list)
        else:
            raise ValueError("you need to load the component first")
            return "error"
        


