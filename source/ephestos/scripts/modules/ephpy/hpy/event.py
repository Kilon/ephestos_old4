"""
IGNORE:
/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */
 IGNORE

The module that handles events


.. inheritance-diagram:: ephpy.hpy.event
    :parts: 1

"""
import bpy
from ephpy import pylivecoding


class EventProcessor(pylivecoding.LiveObject):
    """event processor is handling any form of events
    
    those events can come from any source but is usually a python modal operator that acts as the gateway to blender events. EventProcessor does not create its own events, it mainly handles the existing Blender events passed to it 
    """

    def __init__(
        self,
        name="",
        on_left_click_action=None,
        on_left_click_released_action=None,
        on_right_click_action=None,
        on_right_click_released_action=None,
        on_mouse_in_action=None,
        on_mouse_out_action=None,
        on_drag_action=None,
        on_drop_action=None,
    ):

        self.morph = None
        # essentially these variables enable and disable the handling of specific events. If events are disabled
        # they are ignored by this morph but they do pass to its children. If none handles them as well, the event
        # is passed back to Blender through world's consumed_event instance variable. For more info about this , see
        # World comments
        self.handles_mouse_down = False
        self.handles_events = False
        self.handles_mouse_over = False
        self.handles_drag_drop = False
        self.handles_highlight = False

        # these are actions which are basically simple python objects that contain an appropriate method
        # like on_left_click or on_right_click. This allows us to keep as MVC model that has the handling of
        # events seperate from Hecate and for the user to define his own actions without having to subclass Morph
        self.on_left_click_action = on_left_click_action
        self.on_left_click_released_action = on_left_click_released_action
        self.on_right_click_action = on_right_click_action
        self.on_right_click_released_action = on_right_click_released_action
        self.on_mouse_in_action = on_mouse_in_action
        self.on_mouse_out_action = on_mouse_out_action
        self.on_drag_action = on_drag_action
        self.on_drop_action = on_drop_action

        # drag and drop flag
        self.is_drag_drop = False
        self.drag_position_current = [0, 0]
        self.drag_position_previous = [0, 0]
        self.world_previous_can_hide_state = True

    def clone(self, event_processor_instance):
        """copy itself and its instance variables data
        
        :param EventProcessor event_processor_instance: object to receive copies of the instance variables data
        """

        self.morph = event_processor_instance.morph

        self.handles_mouse_down = event_processor_instance.handles_mouse_down
        self.handles_events = event_processor_instance.handles_events
        self.handles_mouse_over = event_processor_instance.handles_mouse_over
        self.handles_drag_drop = event_processor_instance.handles_drag_drop
        self.handles_highlight = event_processor_instance.handles_highlight

        self.on_left_click_action = event_processor_instance.on_left_click_action
        self.on_left_click_released_action = (
            event_processor_instance.on_left_click_released_action
        )
        self.on_right_click_action = event_processor_instance.on_right_click_action
        self.on_right_click_released_action = (
            event_processor_instance.on_right_click_released_action
        )
        self.on_mouse_in_action = event_processor_instance.on_mouse_in_action
        self.on_mouse_out_action = event_processor_instance.on_mouse_out_action
        self.on_drag_action = event_processor_instance.on_drag_action
        self.on_drop_action = event_processor_instance.on_drop_action

        self.is_drag_drop = event_processor_instance.is_drag_drop
        self.drag_position_current = event_processor_instance.drag_position_current
        self.drag_position_previous = event_processor_instance.drag_position_previous
        self.world_previous_can_hide_state = (
            event_processor_instance.world_previous_can_hide_state
        )

    def process(self, event=None, context=None):
        """ process Blender events

        This is also an internal method called by the World morph, that acts as the general
        mechanism for figuring out the type event it received and sending it to the appropriate
        specialised method. Generally this should not be overridden by your classes unless you
        want to override the general event behavior of the morph. For specific event override the
        relevant methods instead.
        """

        if (
            self.handles_events
            and not self.morph.is_hidden
            and self.morph.can_draw
            and not self.morph.mouse_over_morph
            and self.is_drag_drop
            and event.type == "LEFTMOUSE"
            and event.value == "RELEASE"
        ):
            self.on_left_click_released()
        if (
            self.handles_events
            and not self.morph.is_hidden
            and self.morph.can_draw
            and not self.morph.world.consumed_event
        ):
            if event.type in {"LEFTMOUSE", "RIGHTMOUSE"}:
                self.on_mouse_click(event=event, context=context)

            elif self.handles_mouse_over and event.type == "MOUSEMOVE":
                self.on_mouse_over(event)
        if len(self.morph.children) > 0:
            for morph in self.morph.children:
                morph.event_processor.process(event, context)

    def on_mouse_click(self, event=None, context=None):
        """event when any mouse button is pressed or released"""
        if self.morph.mouse_over_morph and self.handles_mouse_down:
            self.morph.world.consumed_event = True
            if event.type == "LEFTMOUSE" and event.value == "PRESS":
                self.on_left_click()
            if event.type == "LEFTMOUSE" and event.value == "RELEASE":
                self.on_left_click_released()
            if event.type == "RIGHTMOUSE" and event.value == "PRESS":
                self.on_right_click()
            if event.type == "RIGHTMOUSE" and event.value == "RELEASE":
                self.on_right_click_released()

    def on_mouse_over(self, event=None, context=None):
        """event when the mouse cursor passes over the area occupied by the morph"""
        if self.is_drag_drop:
            if self.on_drag_action is not None:
                self.on_drag_action(self.morph)

        if self.morph.mouse_over_morph:
            return self.on_mouse_in()
        else:
            return self.on_mouse_out()

    def on_left_click(self, event=None, context=None):
        """event handling on left mouse button pressed
        the following methods should be self explanatory and depend on the action classes passed to the
        morph. These are also the methods to override if you want to treat specific events differently inside your morph
        """
        if self.on_left_click_action is not None:
            return self.on_left_click_action(self.morph)
        else:
            if not self.is_drag_drop:
                self.is_drag_drop = True
                self.world_previous_can_hide_state = self.morph.world.can_hide
                self.morph.world.can_hide = False
                if not self.morph.world.morph_focused is None:
                    if (
                        not self.morph.world.morph_focused is self.morph
                        and not self.morph.world.morph_focused.event_processor.is_drag_drop
                    ):
                        self.morph.world.morph_focused = self.morph
                else:
                    self.morph.world.morph_focused = self.morph
            return self.morph.world.event

    def on_left_click_released(self, event=None, context=None):
        """handles event when left mouse button is released
        """

        if self.on_left_click_released_action is not None:
            return self.on_left_click_released_action(self.morph)
        else:
            if self.is_drag_drop:
                self.is_drag_drop = False
                self.morph.world.can_hide = self.world_previous_can_hide_state
                if self.on_drop_action is not None:
                    self.on_drop_action(self.morph)
            return self.morph.world.event

    def on_right_click(self, event=None, context=None):
        """handles event when right mouse button is pressed
        """

        if self.on_right_click_action is not None:
            return self.on_right_click_action(self.morph)
        else:
            return self.morph.world.event

    def on_right_click_released(self, event=None, context=None):
        """handles event when right mouse button is released
        """

        if self.on_right_click_released_action is not None:
            return self.on_right_click_released_action(self.morph)
        else:
            return self.morph.world.event

    # an event for when the mouse enters the area of the Morph
    def on_mouse_in(self, event=None, context=None):
        if not self.morph.world.morph_focused is None:
            if (
                not self.morph.world.morph_focused is self.morph
                and not self.morph.world.morph_focused.event_processor.is_drag_drop
            ):
                self.morph.world.morph_focused = self.morph
        else:
            self.morph.world.morph_focused = self.morph
        if self.handles_highlight:
            self.on_mouse_in_highlight()

        if self.on_mouse_in_action is not None:
            return self.on_mouse_in_action(self.morph)
        else:
            return self.morph.world.event

    # an event for when the mouse exits the area of the Morph
    def on_mouse_out(self, event=None, context=None):
        if self.handles_highlight:
            self.on_mouse_out_highlight()
        if self.on_mouse_out_action is not None:
            return self.on_mouse_out_action(self.morph)
        else:
            return self.morph.world.event

    def on_mouse_in_highlight(self):
        if (
            self.handles_highlight
            and self.morph.can_highlight
            and not self.is_drag_drop
        ):
            self.morph.is_highlighted = True

    def on_mouse_out_highlight(self):
        if (
            self.handles_highlight
            and self.morph.can_highlight
            and not self.is_drag_drop
        ):
            self.morph.is_highlighted = False


class KnobEventProcessor(EventProcessor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.on_drag_action is None:
            self.on_drag_action = self.on_drag_default_action

    def clone(self, event_processor_instance):
        super().clone(event_processor_instance)
        if self.on_drag_action is None:
            self.on_drag_action = self.on_drag_default_action

    def on_mouse_over(self, event=None, context=None):
        if event.shift:
            self.morph.is_fine_increment = True
        else:
            self.morph.is_fine_increment = False
        super().on_mouse_over(event=event, context=context)

    def on_drag_default_action(self, morph):
        if self.morph.is_fine_increment:
            increment_step = self.morph.fine_value_increment_step
        else:
            increment_step = self.morph.value_increment_step

        if self.morph.controlled_object == "object":
            # import pdb;pdb.set_trace()
            for ob in bpy.data.objects:
                if ob.select_get() and self.morph.has_to_be_selected:
                    offset = self.calculate_drag_offset()
                    self.morph.value = self.morph.value + (
                        offset * increment_step
                    )
                    self.morph.event_processor.drag_position_previous = (
                        self.morph.event_processor.drag_position_current
                    )
                    exec("ob." + morph.controlled_property + "=morph.value")
                elif not self.morph.has_to_be_selected:
                    offset = self.calculate_drag_offset()
                    self.morph.value = self.morph.value + (
                        offset * increment_step
                    )
                    self.morph.event_processor.drag_position_previous = (
                        self.morph.event_processor.drag_position_current
                    )
                    exec("ob." + morph.controlled_property + "=morph.value")

    def calculate_drag_offset(self):
        if self.morph.event_processor.drag_position_previous == [0, 0]:
            self.morph.event_processor.drag_position_previous = (
                self.morph.world.mouse_position
            )
        self.morph.event_processor.drag_position_current = (
            self.morph.world.mouse_position
        )
        offset = (
            self.morph.event_processor.drag_position_current[1]
            - self.morph.event_processor.drag_position_previous[1]
        )
        if offset > 0:
            offset = 1
        elif offset < 0:
            offset = -1
        return offset


class EditTextEventProcessor(EventProcessor):
    "event processor for input fields"


class TooltipEventProcessor(pylivecoding.LiveObject):
    def __init__(self, name=""):
        self.morph = None
        self.is_drag_drop = False
        self.handles_mouse_down = False
        self.handles_events = True
        self.handles_mouse_over = False
        self.handles_drag_drop = False
        self.handles_highlight = False

    def process(self, event, context):
        if len(self.morph.world.children) > 0:

            if self.morph.world.morph_focused != None:
                self.morph.text = self.morph.world.morph_focused.info_text
                self.morph.position = [
                    0,
                    self.morph.world.morph_focused.position[1] + 50,
                ]
