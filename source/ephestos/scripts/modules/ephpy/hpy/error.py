from ephpy.pylivecoding import LiveValueError


class HecateValueError(LiveValueError):
    """ an object to keep track of errors occuring to the live object
    """

    def __init__(self, message):
        """create value error exception

        :param str message: message of the error 
        """

        self.category = "Hecate"
        self.error_type = "value"
        self.message = f"{self.category} {self.error_type} error: {message}"

        raise ValueError(message)


class HecateIndexError(LiveValueError):
    """ an object to keep track of errors occuring to the live object
    """

    def __init__(self, message):
        """create index error exception

        :param str message: message of the error 
        """

        self.category = "Hecate"
        self.error_type = "index"
        self.message = f"{self.category} {self.error_type} error: {message}"

        raise IndexError(message)


class HecateTypeError(LiveValueError):
    """ an object to keep track of errors occuring to the live object
    """

    def __init__(self, message):
        """create type error exception

        :param str message: message of the error 
        """

        self.category = "Hecate"
        self.error_type = "type"
        self.message = f"{self.category} {self.error_type} error: {message}"

        raise TypeError(message)
