"""
IGNORE:

    /*
    * ***** BEGIN GPL LICENSE BLOCK *****
    *
    * This program is free software; you can redistribute it and/or
    * modify it under the terms of the GNU General Public License
    * as published by the Free Software Foundation; either version 2
    * of the License, or (at your option) any later version.
    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software Foundation,
    * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    *
    * Developer: Dimitris Chloupis
    *
    * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
    * All rights reserved.
    * ***** END GPL LICENSE BLOCK *****
    */
IGNORE

"""

import bpy
from ephpy import pylivecoding
from ephpy.hpy.error import HecateValueError

from . import event as hec_event
from . import layout_engine as hec_layout
from . import texture as hec_texture


class Morph(pylivecoding.LiveObject):

    """The simplest GUI element

    The Morph is extremely essential in Hecate. It provides the base class that  all other morph classes are inherit from. In this class is the most basic functionality of GUI elements that are called "Morphs". Specific functionality  is defined on specific subclasses of this class. Hecate is inspired by Morphic, a GUI that is also based on morphs, approaching GUI creation as a lego like process  of assembling almost identical things together with ease and simplicity

    .. inheritance-diagram:: ephpy.hpy.core.Morph
        :parts: 1

    .. autosummary::

        world
        add_morph
        children
        parent
        position
        world_position
        absolute_position
        width
        height
        text
        info_text
        texture
        colapse
        expand
        mouse_over_morph
        append_to_catalog
        is_hidden
        draw
        x
        y

    """

    def __init__(
        self,
        texture="",
        textures=[""],
        width=36,
        height=36,
        position=[0, 0],
        color=[1.0, 1.0, 1.0, 1.0],
        name="",
        on_left_click_action=None,
        on_left_click_released_action=None,
        on_right_click_action=None,
        on_right_click_released_action=None,
        on_mouse_in_action=None,
        on_mouse_out_action=None,
        on_drag_action=None,
        on_drop_action=None,
        texture_path=None,
        on_draw=None,
        scale=1,
        is_container=False,
        info_text="",
    ):

        """
        Creates the morph. A morph is the most basic graphical element, in this case it represents just an image on screen. A morph is made of textures. A moprh can have multiple textures but only one texture active a time defined by self.active_texture. A texture itself is a collection of layers semi transparent images.

        :param str texture: the name of the icon to be used, see also :py:attr: `~texture`
        :param list(str) textures: more than one texture to be used
        :param int width: width of the morph
        :param int height: height of the morph
        :param list[int,int] position: position of the morph
        :param list[float,float,float,float] color: color of the morph [r,g,b,a]
        :param str name: name of the morph used to identify and locate specific morphs
        :param method([self,]morph) on_left_click_action: a method with signature (self,morph) to be executed when users left clicks on morph
        :param method([self,]morph) on_left_click_released_action: a method to be executed when users released left click on morph
        :param method([self,]morph) on_right_click_action: a method to be executed when users right clicks on morph
        :param method([self,]morph) on_right_click_released_action: a method to be executed when users released right click on morph
        :param method([self,]morph) on_mouse_in_action: a method to be executed when users enters area of morph with mouse
        :param method([self,]morph) on_mouse_out_action: a method to be executed when users exits area of morph with mouse
        :param method([self,]morph) on_mouse_drag_action: a method to be executed when users drags the morph with mouse
        :param method([self,]morph) on_mouse_drops_action: a method to be executed when users ends draging the morph with mouse
        :param method([self,]morph) on_mouse_draw_action: a method to be executed when morph is about to draw itself
        :param str texture_path: the path where the png file can be located
        :param int scale: a number that can be used to multiple width and height
        :param bool(default=false) is_container: whether morph acts as a container for other morphs
        :param str info_text: text usually displayed by tooltip when mouse hovers over morph

        start with creating the morph

        >>> m = Morph()
        >>> print(m.width)
        36

        """

        self._name = name
        self._world = None

        self._text = ""
        # a morph can be inside another morph. That other morph is the parent while this morph becomes the child
        self._parent = None
        self._children = []

        self._width = width

        self._height = height
        self._position = position

        self._info_text = info_text

        self._is_container = is_container
        self._is_expanded = True
        self._is_enabled = False
        self._is_highlighted = False
        self._can_highlight = False
        self._width_old = width
        self._height_old = height
        self._main_morph = None
        self.catalogue_entry_type = "default"

        self.on_draw = on_draw

        # event_processor handles events for morph
        self.event_processor = hec_event.EventProcessor(
            on_left_click_action=on_left_click_action,
            on_left_click_released_action=on_left_click_released_action,
            on_right_click_action=on_right_click_action,
            on_right_click_released_action=on_right_click_released_action,
            on_mouse_in_action=on_mouse_in_action,
            on_mouse_out_action=on_mouse_out_action,
            on_drag_action=on_drag_action,
            on_drop_action=on_drop_action,
            name=f"{self.name}_event_processor",
        )

        self.event_processor.morph = self

        self.color = color

        self._is_hidden = False

        # this counts the amount of times the morph has been drawn. Can be useful to figure out FPS
        # and make sure Hecate does not slow down Blender
        self.draw_count = 0

        # a morph can be scaled like any blender object. The scale is also tied to the scale of the active texture
        # the scale of the active texture depends on the dimensions of the png file
        self._scale = scale

        # this tells where to find the textures
        if texture_path is None:
            self.texture_default_path = hec_texture.Texture.texture_default_path
        else:
            self.texture_path = texture_path

        # active texture is the texture displaying at the time
        # only one texture can display at a time , if you want more then you have to have multiple child morphs
        # each child will have its own active texture
        if texture != "" and texture is not None:
            tex = hec_texture.Texture(name=f"{self.name}_texture")
            tex.add_layer(texture, visibility=True)
            self.active_texture = tex
        else:
            self.active_texture = hec_texture.Texture(name=f"{self.name}_texture")

        if textures != [""]:
            for t in textures:
                self.textures = []
                tex = hec_texture.Texture(name=f"{self.name}_texture")
                tex.add_layer(t)
                self.textures.append(tex)
        else:
            self.textures = [self.active_texture]

        self._can_draw = True
        self._can_hide = True

        self._can_autoresize = False

    #  _____________________________
    # | START CONTAINER PROPERTIES |
    # -----------------------------

    @property
    def is_container(self):
        return self._is_container

    @is_container.setter
    def is_container(self, value):
        if isinstance(value, bool):
            self._is_container = value
            self.can_autoresize = True
        else:
            HecateValueError("is_container can take values that are only True or False")

    @property
    def is_expanded(self):
        return self._is_expanded

    @is_expanded.setter
    def is_expanded(self, value):
        if self._is_container:
            if isinstance(value, bool):
                self._is_expanded = value
            else:
                HecateValueError(
                    "is_expanded can take values that are only True or False"
                )

    @property
    def main_morph(self):
        return self._main_morph

    @main_morph.setter
    def main_morph(self, morph):
        if morph in self.children:
            if isinstance(morph, Morph):
                self._main_morph = morph
            else:
                HecateValueError("main_morph needs to be of class or sublass of Morph")
        else:
            HecateValueError(
                "main_morph must be first added as a child to this morph (see Morph.add_morph(morph))"
            )

    @property
    def can_autoresize(self):
        """ will the morph change size to encapsulate its childre

        By default this is on so that each time a child morph is added, moved or resized , the parent will act accordingly by resizing itself to make sure its never clips any of its children
        
        :param bool value: True or False whether it should autoresize
        :return: see above
        :retype: bool
        """
        return self._can_autoresize

    @can_autoresize.setter
    def can_autoresize(self, value):
        self._can_autoresize = value

    #   ___________________________
    #  | END CONTAINER PROPERTIES |
    #  ---------------------------

    #  __________________________________
    # | START TRANSFORMATION PROPERTIES |
    # ----------------------------------

    @property
    def width(self):
        """width of morph
        
        can only be a positive number
        
        :param int value: width
        :return: width
        :rtype: int
        """
        if self._width < 0:
            HecateValueError("width must not be a negative value")
        else:
            return self._width

    @width.setter
    def width(self, value):
        if value < 0:
            HecateValueError("new value for width must be a positive number")
        else:
            self._width = value
            if self.parent is not None and self.parent.can_autoresize:
                if (self.position[0] + self.width) > self.parent.width:
                    offset = (self.position[0] + self.width) - self.parent.width
                    self.parent.width = self.parent.width + offset

    # height of the morph
    @property
    def height(self):
        """height of morph
      
        can only be a positive number
        
        :param int value: height
        :return: height
        :rtype: int
        """
        if self._height < 0:
            HecateValueError("height must not be a negative value ")
        else:
            return self._height

    @height.setter
    def height(self, value):
        if value < 0:
            HecateValueError("new value for width must be a positive number")
        else:
            self._height = value
            if self.parent is not None and self.parent.can_autoresize:
                if (self.position[1] + self.height) > self.parent.height:
                    offset = (self.position[1] + self.height) - self.parent.height
                    self.parent.height = self.parent.height + offset

    @property
    def position(self):
        """ position is relative to its parent morph

        it can get or set position, if the new position is outside the parent's bounds then it will enlarge the parent bounds so the parent is big enough to cover the area of the child

        :param list value: [x,y] position to set
        :return: position [x,y]
        :rtype: list

        if positioned outside the boundaries of its parent, parent expands to include the child in its area

        >>> m = Moprh()
        >>> y = Morph()
        >>> m.add_morph(y)
        >>> y.position=[300,0]
        >>> m.width 
        336
        
        otherwise it keeps its standard width
        
        >>> m.width = 36
        >>> y.position=[0,0]
        >>> m.width
        36
        """
        return self._position

    @position.setter
    def position(self, value):
        if value[0] < 0 or value[1] < 0:
            HecateValueError("new position must be a list with positive values")
        else:
            if self.parent is not None and self.parent.can_autoresize:
                if (value[0] + self.width) > self.parent.width:
                    self.parent.width = value[0] + self.width
                if (value[1] + self.height) > self.parent.height:
                    self.parent.height = value[1] + self.height
            self._position = value

    @property
    def world_position(self):
        """it returns the position of the morph relative to the world it belongs to
        
        [0,0] is the world position. WARNING !!! READ ONLY PROPERTY, use Morph.position to change position
        :return: position relative to the position of the world 
        :rtype: list[x,y]
        """

        if self.parent is not None:
            return [
                self.parent.world_position[0] + self.position[0],
                self.parent.world_position[1] + self.position[1],
            ]
        else:
            return self.position

    @world_position.setter
    def world_position(self, value):
        HecateValueError("world_position is a read only property")

    # absolute position is the position relative to the entire blender window
    @property
    def absolute_position(self):
        if self.world is not None:
            return [
                self.world_position[0] + self.world.position[0],
                self.world_position[1] + self.world.position[1],
            ]
        else:
            return self.world_position

    # world position is a read only variable
    @absolute_position.setter
    def absolute_position(self, value):
        HecateValueError("absolute_position is read only !")

    # bounds defines the boundary box of the morph
    @property
    def bounds(self):
        return [
            self.position[0],
            self.position[1],
            self.position[0] + self.width,
            self.position[1] + self.height,
        ]

    @bounds.setter
    def bounds(self, value):
        HecateValueError("bounds is read only !")

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = value
        self.width = round(self.width * value)
        self.height = round(self.height * value)
        for morph in self.children:
            morph.scale = value

    #  _______________________________
    # | END TRASFORMATION PROPERTIES |
    # -------------------------------

    #  _______________________________
    # | START INHERITANCE PROPERTIES |
    # -------------------------------

    @property
    def children(self):
        """ returns and sets the children of a morph

        when :py:meth:`add_morph` is used , a child is added to :py:meth:`children` and that child morph has its :py:meth:`parent` set to self.

        :param list(Morph) value: list of morphs as children
        :return: children morphs
        :rtype: list(Morph)
        """
        children = []
        for child in self.world._children:
            if child.parent is self:
                children.append(child)
        return children

    @children.setter
    def children(self, value):
        HecateValueError(
            "children is a read only property , can be modified only indirectly via add_morph or remove_morph"
        )

    # every Morph belongs to a World which is another Morph
    # acting as a general manager of the behavior of Morphs
    @property
    def world(self):
        """ returns or assigns instance of the :py:class:`World` to which this morph belongs"""
        if self._world is None and self._parent is not None:
            self._world = self.parent.world
        return self._world

    @world.setter
    def world(self, value):
        self._world = value

    # a Morph can contain another Morph, if so each morph it contains
    # is called a "child" and for each child it is the parent
    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    #  _______________________________
    # | END INHERITANCE PROPERTIES   |
    # -------------------------------

    #  _______________________________
    # | START TEXT PROPERTIES        |
    # -------------------------------

    @property
    def text(self):
        """this is a :py:class:`str` value that is most frequently used by :py:class:`TextMorph` to determine the text to be displayed but other subclasses could use it as well. """
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @property
    def info_text(self):
        """used by tooltip to descibe the focused morph"""
        return self._info_text

    @info_text.setter
    def info_text(self, value):
        self._info_text = value

    #  _______________________________
    # | END TEXTUAL PROPERTIES       |
    # -------------------------------

    #  _________________________________
    # | START GENERAL STATE PROPERTIES |
    # ---------------------------------

    @property
    def texture(self):
        """the active texture
        
        contains a reference to an instance of :py:class:`ephpy.hpy.texture.Texture` as the active texture used by this morph
         """
        return self.active_texture

    @property
    def mouse_over_morph(self):
        """ checks to see whether the mouse is over this morph  
        
        This property is **ready only** and checks to see when the mouse is over the morph. With standard behaviour it will work as expected but please note """
        sx1 = self.absolute_position[0]
        sy1 = self.absolute_position[1]
        sx2 = self.absolute_position[0] + self.width
        sy2 = self.absolute_position[1] + self.height
        mx = self.world.mouse_position[0]
        my = self.world.mouse_position[1]

        return self.point_inside_area([mx, my], [sx1, sy1, sx2, sy2])

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    @property
    def is_enabled(self):
        return self._is_enabled

    @is_enabled.setter
    def is_enabled(self, value):
        if isinstance(value, bool):
            self._is_enabled = value
        else:
            HecateValueError("is_enabled can take values that are only True or False")

    #  _________________________________
    # | END GENERAL STATE PROPERTIES   |
    # ---------------------------------

    #  _________________________________
    # | START VISIBILITY PROPERTIES    |
    # ---------------------------------

    @property
    def can_hide(self):
        return self._can_hide

    @can_hide.setter
    def can_hide(self, value):
        if isinstance(value, bool):
            self._can_hide = value
        else:
            HecateValueError("can_hide can only accept values True or False")

    @property
    def can_draw(self):
        return self._can_draw

    @can_draw.setter
    def can_draw(self, value):
        if isinstance(value, bool):
            self._can_draw = value
        else:
            HecateValueError("can_draw can only accept values True or False")

    @property
    def can_highlight(self):
        return self._can_highlight

    @can_highlight.setter
    def can_highlight(self, value):
        if isinstance(value, bool):
            self._can_highlight = value
        else:
            HecateValueError("can_highlight can only accept values True or False")

    @property
    def is_hidden(self):
        return self._is_hidden

    @is_hidden.setter
    def is_hidden(self, value):
        if isinstance(value, bool):
            if self.can_hide:
                self._is_hidden = value
            else:
                HecateValueError("cannot hide a morph if can_hide is False")
        else:
            HecateValueError("is_hidden can only accept True or False values")

    @property
    def is_highlighted(self):
        return self._is_highlighted

    @is_highlighted.setter
    def is_highlighted(self, value):
        if isinstance(value, bool):
            if self.can_highlight:
                self._is_highlighted = value
            else:
                HecateValueError("cannot highlight a morph if can_highlight is False")
        else:
            HecateValueError("is_highlighted can only accept True or False values")

    #  _________________________________
    # | END VISIBILITY PROPERTIES      |
    # ---------------------------------

    #  _________________________________
    # | START TEXTURE PROPERTIES       |
    # ---------------------------------

    @texture.setter
    def texture(self, texture):
        self.active_texture = texture
        for tex in self.textures:
            if tex is texture:
                return "texture_found"
        self.textures.append(texture)
        return "texture_appended"

    #  _________________________________
    # | START METHODS                  |
    # ---------------------------------

    def point_inside_area(self, point, area):
        """check to see whether a particular point is inside an area

        :param list[int,int] point: a list of [x,y] format
        :param list[int,int,int,int] area: a list of [x,y,x+width,y+height]
        :return: True or False
        :rtype: bool
        """

        if (
            point[0] >= area[0]
            and point[0] <= area[2]
            and point[1] >= area[1]
            and point[1] <= area[3]
        ):
            return True
        else:
            return False

    def is_overlapping_morph(self, morph):
        """check to see whether part of the area of another morph is inside its area
        
        
        :param Morph morph: the morph to check if inside the area
        :return: True or False
        :rtype: bool"""

        # going anti-clockwise
        # morph bottom left corner
        m_bl = [morph.absolute_position[0], morph.absolute_position[1]]

        # morph top right corner
        m_tr = [
            morph.absolute_position[0] + morph.width,
            morph.absolute_position[1] + morph.height,
        ]

        # self bottom left corner
        s_bl = [self.absolute_position[0], self.absolute_position[1]]
        # self bottom right corner
        s_br = [self.absolute_position[0] + self.width, self.absolute_position[1]]
        # self top right corner
        s_tr = [
            self.absolute_position[0] + self.width,
            self.absolute_position[1] + self.height,
        ]
        # self top left corner
        s_tl = [self.absolute_position[0], self.absolute_position[1] + self.height]

        morph_area = [m_bl[0], m_bl[1], m_tr[0], m_tr[1]]

        self_bottom_left_inside = self.point_inside_area(s_bl, morph_area)
        self_bottom_right_inside = self.point_inside_area(s_br, morph_area)
        self_top_right_inside = self.point_inside_area(s_tr, morph_area)
        self_top_left_inside = self.point_inside_area(s_tl, morph_area)

        return (
            self_bottom_left_inside
            or self_bottom_right_inside
            or self_top_right_inside
            or self_top_left_inside
        )

    def is_inside_morph(self, morph):
        """check to see whether the entire area of another morph is inside its area
        
        by inside is not meant that the other morph merely overlaps or that is a child , only that the entire area of the morph is inside
        
        :param Morph morph: the morph to check if inside the area
        :return: True or False
        :rtype: bool"""

        # going anti-clockwise
        # morph bottom left corner
        m_bl = [morph.absolute_position[0], morph.absolute_position[1]]
        # morph top right corner
        m_tr = [
            morph.absolute_position[0] + morph.width,
            morph.absolute_position[1] + morph.height,
        ]

        # self bottom left corner
        s_bl = [self.absolute_position[0], self.absolute_position[1]]
        # self bottom right corner
        s_br = [self.absolute_position[0] + self.width, self.absolute_position[1]]
        # self top right corner
        s_tr = [
            self.absolute_position[0] + self.width,
            self.absolute_position[1] + self.height,
        ]
        # self top left corner
        s_tl = [self.absolute_position[0], self.absolute_position[1] + self.height]

        morph_area = [m_bl[0], m_bl[1], m_tr[0], m_tr[1]]

        self_bottom_left_inside = self.point_inside_area(s_bl, morph_area)
        self_bottom_right_inside = self.point_inside_area(s_br, morph_area)
        self_top_right_inside = self.point_inside_area(s_tr, morph_area)
        self_top_left_inside = self.point_inside_area(s_tl, morph_area)

        return (
            self_bottom_left_inside
            and self_bottom_right_inside
            and self_top_right_inside
            and self_top_left_inside
        )

    # collapse morph if it is a container
    def colapse(self):
        """this is used only for morphs that are containers. It minimises the moprh by reducing its width and height to its first child which is also the only child that remains visible. If you wish to completely hide the container morph thet you should use :py:attr:`~.is_hidden`"""
        if self.main_morph is not None and self.is_container:
            self.width = self.main_morph.width + self.main_morph.position[0]
            self.height = self.main_morph.height + self.main_morph.position[1]
            for child in self.children:
                if child is not self.main_morph:
                    child.can_draw = False
            self.is_expanded = False
            return "success"
        return "failure"

    # expand morph if it is a container
    def expand(self):
        """this is used only for moprhs that are containers. It maximises the size of the container morph making all the moprh it contains visible. The moprh it contains are its children."""
        if self.main_morph is not None and self.is_container:
            for child in self.children:
                if child is not self.main_morph:
                    child.can_draw = True
                    child.position = child._position
            self.is_expanded = True
        return "success"

    # one texture can be active at the time in order to display on screen
    def activate_texture_index(self, index):
        """this method activates a specific texture from a specific index of the textures list.

        :type index: integer
        :param index: a number that is the index of the texture to activate. Only one texture can be active at a time"""
        if index < len(self.textures):
            self.active_texture = self.textures[index]
        else:
            HecateValueError("texture index given is too large")
        return "success"

    def activate_texture(self, texture):
        """this method uses a texture as the active texture referenced by :py:attr:`~.active_texture`, and adds it to :py:attr:`~.textures` list.

        :type texture: Texture
        :param texture: the texture to be used as active texture  """
        for tex in self.textures:
            if tex is texture:
                self.active_texture = texture
                return self
            else:
                HecateValueError("texture cannot be found")
        return "success"

    def add_texture(self, texture):
        """this method adds a texture to :py:attr:`~.textures` list

        :type texture: Texture
        :param texture: the texture to be added  """
        for tex in self.textures:
            if tex is texture:
                HecateValueError("duplicate texture")
        self.textures.append(texture)
        return "success"

    def remove_texture(self, texture):
        """finds the texture in :py:attr:`.textures` list and removes it

        :type texture: Texture
        :param texture: the texture to be removed"""
        is_a_texture_deleted = False
        for tex in self.textures:
            if tex is texture:
                del tex
                is_a_texture_deleted = True
        if is_a_texture_deleted:
            return "success"
        else:
            HecateValueError("could not locate the texture to be removed from textures")

    def add_morph(self, morph):
        """add a morph as a child 

        the child makes self as parent Morph

        :param Morph morph: the morph to be added as child
        
        add a child morph to it

        >>> m = Morph()
        >>> y = Morph()
        >>> m.add_morph(y)
        >>> y.parent is m
        True

        """
        morph.parent = self
        if self.world is not None:
            self.world._children.append(morph)

            morph.position = morph._position
        else:
            HecateValueError(
                "cannot add a morph to another morph without the parent having a world first"
            )

    def get_morph_named(self, name):
        result = None
        if self.world is not None:
            for child in self.world._children:
                if child.name == name:
                    result = child
                    break
        return result

    # this method can be overriden to do special actions during drawing morphs
    def draw(self):
        if self.on_draw is not None:
            self.on_draw(self)
        if (
            not self.is_hidden
            and self.can_draw
            and not self.parent.is_hidden
            and self.parent.can_draw
        ):
            self.append_to_catalog()
            for morph in self.children:
                if morph.is_inside_morph(self):
                    if self.world.is_clipping and morph.is_overlapping_morph(
                        self.world
                    ):
                        morph.draw()
                    elif not self.world.is_clipping:
                        morph.draw()
        return

    # internal method , don't use uless you understand what you are doing
    # this method will trigger the Hecate ACL mechanism for loading the images
    # defined as layers for the active_texture as long as the layer is visible.
    # There can be only one active texture at a time.

    def append_to_catalog(self):
        if not self.is_hidden and self.can_draw:
            for x in range(0, len(self.active_texture.layers)):
                if self.active_texture.layer_visibility[x]:
                    self.world.modal_operator.ephestos_component.request(
                        "hec_morph_catalog_append",
                        [
                            self.world_position[0],
                            self.world_position[1],
                            self.width,
                            self.height,
                            self.active_texture.layers[x],
                            self.catalogue_entry_type,
                            2,
                            "",
                            self.text,
                        ],
                    )
        return "success"

    def morph_overlaps_child(self, morph):
        for morph_child in self.children:
            if (
                morph.position[0] > morph_child.position[0]
                and morph.position[0] < morph_child.position[0] + morph_child.width
                and morph.position[1] > morph_child.position[1]
                and morph.position[1] < morph_child.position[1] + morph_child.height
            ):
                return morph_child
        return None

    # upper left corner of the bounding box
    def x(self):
        return self.position[0]

    def y(self):
        return self.position[1]

    # lower right corner of the bounting box, defining the area occupied by the morph
    def x2(self):
        return self.x() + self.width

    def y2(self):
        return self.y() + self.height


class World(Morph):
    """World is a singleton main container and manager for all morphs

    A World morph instance is a morph that is the parent of all top parent morphs and as such it acts as the container of all morphs. It's main purpose to make sure morphs are in their right position , are drawn properly and their events are handled. If an event is not consumed by a morph then it is returned back to Blender to be handled by Blender's normal functionalities depending on the event. If a event is consumed by a morph , which means it uses it for its internal functionality, then the even is not returned back to Blender and as such for Blender will be as the event never happened. Morph generally handle mouse events like left and right mouse clicks as well as mouse movement, but they are capable of handling keyboard events as well.

    A morph returns and assign its world via :py:attr:`Morph.world`
    
    A world instance is simple morph that triggers and handles the drawing methods and event methods for each child morph. In order for a morph to be a child of a World it has to be added to it or else it wont display. There can be more than one world. Generally this is not necessary if you want to create a multi layer interfaces because each morph can act as a container (parent) to other morphs (children). On the other hand there are cases when you want each layer to be really separate and with its own handling of events and drawing which make sense to have multiple worlds. The choice is up to you but remember you have to call on_event method for each world you have if you want that world to display and handle events for its children morphs.A world requires a modal operator , because only Blender's modal operators are the recommended way for handling Blender events and drawing on regions of internal Blender windows. As such the draw() method must be called inside the method associated with the modal's drawing and on_event is called on the modal method of your modal operator. You need to call only those two methods for Hecate to work of course taking into account you have already created a world , creted the morphs and added the morphs to the world via add_morph method.
    
    .. inheritance-diagram:: ephpy.hpy.core.World
        :parts: 1

    """

    def __init__(self, *args, **kwargs):
        """create the world see :py:class:`Morph.__init__` for more details """
        super().__init__(*args, **kwargs)

        # this defines whether the event send to World's onEvent method
        # has been handled by any morph. If it has not , you can use this variable
        # to make sure your modal method returns {"PASS_THROUGH"} so that the event
        # is passed back to Blender and you don't block user interaction
        self.consumed_event = False

        # the modal operator that uses this World
        self.modal_operator = None

        # the coordinates of the mouse cursor, its the same as blender mouse coordinates
        # of the WINDOW region of the internal window that has been assigned the modal
        # operator needed to draw and send events to Hecate. Blender does not change that
        # window, so the mouse coordinates start [0,0] does not change as well
        self.mouse_position = [0, 0]

        # used to store how many pixels outside the world will mouse have to be to close the menu
        self.mouse_limit = 10

        # whether mouse is inside a region that is drawing at the time
        # this is used for auto_hide feature
        self.mouse_cursor_inside = False

        self.region_height = 100
        self.region_width = 100
        self.region_position = [0, 0]

        # the blender event as it is
        self.event = None
        self.context = None

        self.can_draw = True

        # Keep a log of anything happening with Ephestos (Hecate/Aura)
        self.log = None

        self.morph_focused = None

        self.layout_engine = None
        self.refresh_layout_on_event = True
        self.layout_refresh_rate = 0.1

        self.addon_preferences = None
        self._previous_time = None
        self.should_fill = False

        self._is_clipping = False

    @property
    def is_clipping(self):
        return self._is_clipping

    @is_clipping.setter
    def is_clipping(self, value):
        if isinstance(value, bool):
            self._is_clipping = value
        else:
            HecateValueError("is_clipping can only take values True or False")

    # world position returns the position of the morph relative to the world it belongs too
    @property
    def world_position(self):
        return [0, 0]

    @property
    def mouse_over_morph(self):
        apx1 = self.position[0]
        apy1 = self.position[1]
        apx2 = self.position[0] + self.width
        apy2 = self.position[1] + self.height
        ex = self.mouse_position[0]
        ey = self.mouse_position[1]
        result = ex > apx1 and ex < apx2 and ey > apy1 and ey < apy2
        return result

    @property
    def height(self):
        if self._height < 0:
            HecateValueError("height must not be a negative value ")
        else:
            return self._height

    @height.setter
    def height(self, value):
        if value < 0:
            HecateValueError("new value for width must be a positive number")
        else:
            self._height = value

    @property
    def parent(self):
        return None

    @parent.setter
    def parent(self, value):
        HecateValueError("a world cannot not have a parent")

    @property
    def world(self):
        return None

    @world.setter
    def world(self, value):
        HecateValueError("a world cannot not have a world")

    @property
    def children(self):
        """ returns and sets the children of a morph

        when :py:meth:`add_morph` is used , a child is added to :py:meth:`children` and that child morph has its :py:meth:`parent` set to self.

        :param list(Morph) value: list of morphs as children
        :return: children morphs
        :rtype: list(Morph)
        """
        children = []
        for child in self._children:
            if child.parent is self:
                children.append(child)
        return children

    @children.setter
    def children(self, value):
        HecateValueError(
            "children is a read only property , can be modified only indirectly via add_morph or remove_morph"
        )

    # a world cannot have a world by itself and of course not a parent
    # this is why we override the Morph add_morph method
    def add_morph(self, morph):
        self._children.append(morph)
        morph.parent = self
        morph.world = self

    def get_morph_named(self, name):
        result = None

        for child in self._children:
            if child.name == name:
                result = child
                break
        return result

    def terminate(self):
        """ Terminates the execution of the world, 
        
        usually by terminating the python modal operator accessed via self.modal_operator"""

        self.modal_operator.is_running = False
        self.is_hidden = True
        #self.generate_morph_catalog()

    def check_addon_preferences(self):
        """ checks whether it should refresh world
        
        It checks to see if addon preferences are enabled for hec_gui and whether it should refresh itself"""
        if self.addon_preferences is not None:
            pref = self.addon_preferences
            if pref.layout_enable:
                if pref.layout_refresh:
                    self.reflesh_layout_on_event = pref.layout_auto_refresh
                    self.layout_refresh_rate = pref.layout_refresh_rate

    def on_event(self, event=None, context=None):
        """manages blender events
        
        this is usually tied to a python modal operator which will pass the event and the context and that the world morph will administer the events to all its children which then handle them
        
        :param event event: a Blender event object 
        :param conext context: a Blender context
        """

        self.check_addon_preferences()
        if context is not None:
            self.context = context
        else:
            HecateValueError("value for context cannot be None")

        if event is not None:
            self.event = context
        else:
            HecateValueError("value for event cannot be None")

        self.event = event
        bmx = context.region.x
        bmy = context.region.y
        self.region_position = (bmx, bmy)

        self.region_width = context.region.width
        self.region_height = context.region.height

        self.mouse_position = [
            event.mouse_region_x + context.region.x,
            event.mouse_region_y + context.region.y,
        ]
        # consume_event is reset so World does not block events that are not handled by it
        # instead those events are passed back to Blender through the {'PASS_THROUGH'} return
        # so you need to check out this variable and if it is False you need to make sure
        # the modal method of your modal operator (needed for Hecate to work)
        # returns {'PASS_THROUGH'} if you want your user to interact with
        # a Hecate GUI and Blender at the same time or else you will have an angry user hunting you down in forums

        self.consumed_event = False

        if not self.is_hidden:
            for morph in self.children:
                morph.event_processor.process(event, context)
            if event.type == "LEFTMOUSE" and event.value == "PRESS":
                if self.addon_preferences.dev_mode:
                    self.world.modal_operator.log_debug.debug(
                        f"Was event consumed ? {self.consumed_event}"
                    )
        self.generate_morph_catalog()

    def keep_bounds_inside_region(self):
        """ makes sure that world snaps inside the region

        This means basically that something like the icon menu wont leave the region it was triggered from usually the 3d viewport"""

        if self.position[0] + self.width > self.region_position[0] + self.region_width:
            self.position[0] = (
                self.region_position[0] + self.region_width
            ) - self.width

        if (
            self.position[1] + self.height
            > self.region_position[1] + self.region_height
        ):
            self.position[1] = (
                self.region_position[1] + self.region_height
            ) - self.height

        if self.width > self.region_width:
            self.width = self.region_width
        if self.height > self.region_height:
            self.height = self.region_height

    # send a list of morphs to the ephestos.eac for prepartion to draw them
    def generate_morph_catalog(self):
        """ passes info to hecate.acl to draw morphs

        this is responsible for informing at C side , via Hecate's Aura Component Library, which morphs should be drawn and which should not, to keep performance high and avoid unecessary loading of useless png files """

        if not self.is_hidden and self.can_draw:
            self.keep_bounds_inside_region()
            self.modal_operator.ephestos_component.request(
                "hec_morph_catalog_initialise", []
            )
            self.modal_operator.ephestos_component.request(
                "hec_world_set",
                [
                    self.position[0],
                    self.position[1],
                    self.width,
                    self.height,
                    self.region_position[0],
                    self.region_position[1],
                    self.region_width,
                    self.region_height,
                    self.mouse_position[0],
                    self.mouse_position[1],
                    int(self.is_hidden),
                ],
            )

            for morph in self.children:
                morph.draw()
            self.modal_operator.ephestos_component.request("hec_morph_catalog_end", [])
        else:
            self.modal_operator.ephestos_component.request(
                "hec_morph_catalog_initialise", []
            )
            self.modal_operator.ephestos_component.request(
                "hec_world_set",
                [
                    self.position[0],
                    self.position[1],
                    self.width,
                    self.height,
                    self.region_position[0],
                    self.region_position[1],
                    self.region_width,
                    self.region_height,
                    self.mouse_position[0],
                    self.mouse_position[1],
                    int(self.is_hidden),
                ],
            )
            self.modal_operator.ephestos_component.request("hec_morph_catalog_end", [])


class ButtonMorph(Morph):
    """ a ButtonMorph is a morph that responds to an action. This is a default behavior for morphs, however ButtonMorph makes it a bit easier and provides an easy way to change the morph appearance when the mouse is hovering over the button"""

    def __init__(self, *args, **kwargs):
        """ creates button """
        super().__init__(*args, **kwargs)
        self.event_processor.handles_mouse_over = True
        self.event_processor.handles_events = True
        self.event_processor.handles_mouse_down = True
        self.event_processor.handles_highlight = True

    def change_appearance(self, value):
        # if "back" in self.texture:
        if value == 0 and self.active_texture:
            self.active_texture.layer_visibility[0] = True
            self.active_texture.layer_visibility[1] = False
        if value == 1 and self.active_texture:
            self.active_texture.layer_visibility[0] = False
            self.active_texture.layer_visibility[1] = True


class SwitchButtonMorph(ButtonMorph):
    """ Switch Button that can act also together with other connected morphs
    so only one of them can be active at a time"""

    def __init__(self, hover_glow_mode=True, *args, **kwargs):
        """creates switch button """
        super().__init__(*args, **kwargs)
        self.connected_morphs = []
        self.can_highlight = True

    def add_connection(self, morph):
        if morph.__class__ is self.__class__:
            for cmorph in self.connected_morphs:
                if cmorph is morph:
                    return "morph_already_connected"
            self.connected_morphs.append(morph)
            for cmorph in morph.connected_morphs:
                if cmorph is self:
                    return "self_already_connected"
            morph.connected_morphs.append(self)
            return "success"
        else:
            HecateValueError(
                "cannot connect a SwitchButtonMorph to a morph that is not also SwitchButtonMorph"
            )

    def remove_connection(self, morph):
        for con in self.connected_morphs:
            if con is morph:
                for scon in morph.connected_morphs:
                    if scon is self:
                        del scon
                del con

    @property
    def is_highlighted(self):
        return self._is_highlighted

    @is_highlighted.setter
    def is_highlighted(self, value):
        if isinstance(value, bool):
            if value:
                if self.is_enabled:
                    self._is_highlighted = True
                    self.change_appearance(1)
                    for con in self.connected_morphs:
                        con.is_highlighted = False
                else:
                    self._is_highlighted = True
                    self.change_appearance(1)
            else:
                if not self.is_enabled:
                    self._is_highlighted = False
                    self.change_appearance(0)
        else:
            HecateValueError("is_highlighted can take values only of True and False")

    @property
    def is_enabled(self):
        return self._is_enabled

    @is_enabled.setter
    def is_enabled(self, value):
        if isinstance(value, bool):
            if value:
                self.is_highlighted = True
                self._is_enabled = True
                for con in self.connected_morphs:
                    con.is_enabled = False
            else:
                self._is_enabled = False


class TextMorph(Morph):
    """a class that defines a simple label 
    
    This can be a piece of text of any size """

    def __init__(self, font_id=0, text="", size=16, dpi=72, *args, **kwargs):
        "creates the text"
        super().__init__(*args, **kwargs)
        self.size = size
        self.dpi = dpi
        self.text = text
        self.font_id = font_id
        self.text_lines = self.text.splitlines()
        self.catalogue_entry_type = "label"

    def append_to_catalog(self):
        self.world.modal_operator.ephestos_component.request(
            "hec_morph_catalog_append",
            [
                self.world_position[0],
                self.world_position[1],
                self.width,
                self.height,
                "",
                self.catalogue_entry_type,
                2,
                "",
                self.text,
            ],
        )


class EditTextMorph(TextMorph):
    """a class that defines an input field

    this field can contain any form of data but it is always converted to string"""


class KnobMorph(ButtonMorph):
    """ a knob is basically a cicrular slider
    
    Inspired by knobs found in audio hardware and software 
    """

    def __init__(self, font_id=0, text="", size=16, dpi=72, *args, **kwargs):
        "creates a knob morph"
        super().__init__(*args, **kwargs)
        old_event_processor = self.event_processor
        self.event_processor = hec_event.KnobEventProcessor()
        self.event_processor.clone(old_event_processor)
        del old_event_processor
        self.event_processor.handles_events = True
        self.event_processor.handles_drag_drop = True
        self.event_processor.handles_mouse_down = True
        self.event_processor.handles_mouse_over = True
        self.event_processor.handles_highlight = True

        # value controled by the knob
        self._value = 0

        # value multiplier when we we want the knob to increase faster or slower it can also have negative values if we want to reverse the behavior of the knob
        self._value_increment_step = 1
        self._is_fine_increment = False
        self._fine_value_increment_step = 0.1
        # use value None to represent infinity for min and max values
        self._max_value = None
        self._min_value = None

        # here we define which Blender property the knob controls , what kind of object it is and whether it has to be already selected or activated by the user
        self.controlled_property = None
        self.controlled_object = None
        self.has_to_be_selected = False
        self.has_to_be_active = False

    @property
    def max_value(self):
        return self._max_value

    @max_value.setter
    def max_value(self, val):
        self._max_value = val

    @property
    def min_value(self):
        return self._min_value

    @min_value.setter
    def min_value(self, val):
        self._min_value = val

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, val):
        if self._max_value is not None and val > self._max_value:
            self._value = self._max_value
        elif self._min_value is not None and val < self._min_value:
            self._value = self._min_value
        else:
            self._value = val

    @property
    def value_increment_step(self):
        return self._value_increment_step

    @value_increment_step.setter
    def value_increment_step(self, value):
        self._value_increment_step = value

    @property
    def fine_value_increment_step(self):
        return self._fine_value_increment_step

    @fine_value_increment_step.setter
    def fine_value_increment_step(self, value):
        self._fine_value_increment_step = value

    @property
    def is_fine_increment(self):
        return self._is_fine_increment

    @is_fine_increment.setter
    def is_fine_increment(self, value):
        if isinstance(value, bool):
            self._is_fine_increment = value
        else:
            HecateValueError("is_fine_increment can only take values of True or False")

    def activate_texture_layer(self):
        layer = int(self.value + 2)
        self._switch_knob_texture_layer(layer)

    def _switch_knob_texture_layer(self, layer):
        if layer > 101:
            layer = 101
        if layer < 3:
            layer = 2
        if layer < 12:
            self.active_texture.make_layer_visible(
                "/knob_bar/knob_bar_frame_000" + str(layer - 2) + ".png"
            )
        else:
            self.active_texture.make_layer_visible(
                "/knob_bar/knob_bar_frame_00" + str(layer - 2) + ".png"
            )
        for x in range(0, 100):
            if x != layer - 2 and x < 10 and x > -10:
                self.active_texture.make_layer_invisible(
                    "/knob_bar/knob_bar_frame_000" + str(x) + ".png"
                )
            elif x != layer - 2:
                self.active_texture.make_layer_invisible(
                    "/knob_bar/knob_bar_frame_00" + str(x) + ".png"
                )

    def on_draw_default_action(self):
        if self.controlled_object == "object":
            for ob in bpy.data.objects:
                if ob.select_get() and self.has_to_be_selected:
                    self.value = eval("ob."+self.controlled_property)

    def draw(self):
        if self.on_draw is not None:
            self.on_draw()
        else:
            self.on_draw_default_action()
        self.activate_texture_layer()
        if (
            not self.is_hidden
            and self.can_draw
            and not self.parent.is_hidden
            and self.parent.can_draw
        ):
            self.append_to_catalog()
        for morph in self.children:
            if morph.is_inside_morph(self):
                if self.world.is_clipping and morph.is_overlapping_morph(self.world):
                    morph.draw()
                elif not self.world.is_clipping:
                    morph.draw()


class TooltipMorph(KnobMorph):
    """ a morph that acts as a tooltip"""

    def __init__(self, font_id=0, text="", size=16, dpi=72, *args, **kwargs):
        """creates a tooltip"""
        super().__init__(*args, **kwargs)
        self.event_processor = hec_event.TooltipEventProcessor()
        self.event_processor.morph = self
        self.is_hidden = False
        self.text = ""
        self.mode = "passive"

    def append_to_catalog(self):
        if not self.is_hidden and self.can_draw:
            for x in range(0, len(self.active_texture.layers)):
                if self.active_texture.layer_visibility[x]:
                    self.world.modal_operator.ephestos_component.request(
                        "hec_morph_catalog_append",
                        [
                            self.world_position[0],
                            self.world_position[1],
                            self.width,
                            self.height,
                            self.active_texture.layers[x],
                            "default",
                            2,
                            "",
                            "",
                        ],
                    )
            self.world.modal_operator.ephestos_component.request(
                "hec_morph_catalog_append",
                [
                    self.world_position[0],
                    self.world_position[1] + 10,
                    190,
                    18,
                    "",
                    "label",
                    2,
                    "",
                    self.text,
                ],
            )
        return "success"

    def draw(self):
        if self.on_draw is not None:
            self.on_draw(self)
        if (
            not self.is_hidden
            and self.can_draw
            and not self.parent.is_hidden
            and self.parent.can_draw
        ):
            self.append_to_catalog()
        for morph in self.children:
            if self.world.is_clipping and morph.is_overlapping_morph(self.world):
                morph.draw()
            elif not self.world.is_clipping:
                morph.draw()


class LineBarMorph(KnobMorph):
    """part of TooltipMorph
    
    it displayes a horizontal slider for knob morphs"""

    def __init__(self, font_id=0, text="", size=16, dpi=72, *args, **kwargs):
        "creates the line bar morph"
        super().__init__(*args, **kwargs)
        self.event_processor = hec_event.EventProcessor()
        self.event_processor.morph = self
        self.is_hidden = True
        self.parent_initial_width = 0

    def activate_texture_layer(self):
        if self.world.morph_focused is not None:
            if isinstance(self.world.morph_focused, KnobMorph):
                layer = int(self.world.morph_focused.value)
                tooltip_focused_knob_value_morph = self.world.get_morph_named(
                    "focused knob value text"
                )
                tooltip_focused_knob_value_morph.text = str(
                    round(self.world.morph_focused.value, 3)
                )
                self.is_hidden = False
                if self.parent_initial_width != 0:
                    self.parent.width = self.position[0] + self.width
            else:
                self.is_hidden = True
                if self.world.layout_engine is not None:
                    for morph_dict in self.world.layout_engine.json_content["morphs"]:
                        if "name" in morph_dict.keys():
                            if self.parent.name == morph_dict["name"]:
                                self.parent.width = morph_dict["width"]
                                self.parent_initial_width = self.parent.width
                layer = 0
        else:
            layer = 0
        self._switch_knob_texture_layer(layer)

    def _switch_knob_texture_layer(self, layer):
        if layer > 100 or layer < -100:
            layer = 100
        if layer < 0:
            layer = layer * -1
        if layer < 10:
            self.active_texture.make_layer_visible(
                "/line_bar/line_bar_frame_000" + str(layer) + ".png"
            )
        elif layer == 100:
            self.active_texture.make_layer_visible(
                "/line_bar/line_bar_frame_0" + str(layer) + ".png"
            )
        else:
            self.active_texture.make_layer_visible(
                "/line_bar/line_bar_frame_00" + str(layer) + ".png"
            )
        for x in range(0, 101):
            if x != layer and x < 10:
                self.active_texture.make_layer_invisible(
                    "/line_bar/line_bar_frame_000" + str(x) + ".png"
                )
            elif x != layer and x == 100:
                self.active_texture.make_layer_invisible(
                    "/line_bar/line_bar_frame_0" + str(x) + ".png"
                )
            elif x != layer:
                self.active_texture.make_layer_invisible(
                    "/line_bar/line_bar_frame_00" + str(x) + ".png"
                )

    def draw(self):
        if self.on_draw is not None:
            self.on_draw(self)
        self.activate_texture_layer()

        if (
            not self.is_hidden
            and self.can_draw
            and not self.parent.is_hidden
            and self.parent.can_draw
        ):
            self.append_to_catalog()
        for morph in self.children:
            if morph.is_inside_morph(self):
                if self.world.is_clipping and morph.is_overlapping_morph(self.world):
                    morph.draw()
                elif not self.world.is_clipping:
                    morph.draw()
