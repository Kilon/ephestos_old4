"""
IGNORE:
    /*
    * ***** BEGIN GPL LICENSE BLOCK *****
    *
    * This program is free software; you can redistribute it and/or
    * modify it under the terms of the GNU General Public License
    * as published by the Free Software Foundation; either version 2
    * of the License, or (at your option) any later version.
    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software Foundation,
    * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
    *
    * Developer: Dimitris Chloupis
    * 
    * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
    * All rights reserved.
    * ***** END GPL LICENSE BLOCK *****
    */
IGNORE


module for live coding with python

.. inheritance-diagram:: ephpy.pylivecoding.LiveObject ephpy.pylivecoding.LiveEnvironment
    :parts: 1


"""
import importlib,pdb,sys,traceback,inspect,re,collections,time

class LiveObject(object):
    """ an object that can reload its code

    LiveObject registers itself and its module to LiveEnvironment each time a new instance of this class is created. It also has a custom made error mechanism to keep track of past errors. This can be useful when livecoding and the user wants to debug those errors much later on
    """

    def __new__(cls,*args, **kwargs):
        """ add new instance to LiveEnvironment for later reload """
               
        instance = object.__new__(cls)
        LiveEnvironment.register_class(cls.__module__,cls,instance)
        return instance
        
class LiveEnvironment(LiveObject):
    """ it keeps track of all live objects

    as well as the module they belong to"""
    
    live_modules={}
    live_instances={}
    
    @staticmethod
    def register_module(name):
        """ registers a module for live code reload

        :param str name: name of module
        """

        if name not in LiveEnvironment.live_modules.keys():
            LiveEnvironment.live_modules[name]=[]

    @staticmethod
    def register_class(module,live_class,live_instance):
        """register the class of the live object for code reload

        :param LiveObject live_class: class of live object
        :param LiveObject live_instance: live object itself
        """

        LiveEnvironment.register_module(module)
        if not live_class in LiveEnvironment.live_modules[module]:
            LiveEnvironment.live_modules[module].append(live_class)
        if not live_class.__name__ in LiveEnvironment.live_instances.keys():
            LiveEnvironment.live_instances[live_class.__name__]=[]
        if not live_instance in LiveEnvironment.live_instances[live_class.__name__]:
            LiveEnvironment.live_instances[live_class.__name__].append(live_instance) 
    
    @staticmethod
    def update():
        """reload modules registered

        at the same time replace the class of live object instances with the reloaded LiveObject class. This is something that importlib does not do and this class does. Use this method for each time you want to reload your code after you save it over the original file of the module""" 

        for mod in LiveEnvironment.live_modules.keys(): #the name of the module is used as a key for the dict that stores all
            if mod == __name__:
                oldLiveEnvironment = LiveEnvironment
                importlib.reload(sys.modules[mod])
                LiveEnvironment.live_instances = oldLiveEnvironment.live_instances
                LiveEnvironment.live_modules = oldLiveEnvironment.live_modules
                del oldLiveEnvironment
            else:
                importlib.reload(sys.modules[mod])
            for x in range(0,len(LiveEnvironment.live_modules[mod])):
                old_class = LiveEnvironment.live_modules[mod][x]
                new_live_class = eval("sys.modules[\""+mod+"\"]." + old_class.__name__)
                LiveEnvironment.live_modules[mod][x]=new_live_class
                for live_instance in LiveEnvironment.live_instances[new_live_class.__name__]:
                    live_instance.__class__=new_live_class
                    if "instances" in live_instance.__class__.__dict__:
                        live_instance.__class__.instances.append(live_instance)
                    else:
                        live_instance.__class__.instances = [live_instance]
                    
                del old_class


class LiveValueError(LiveObject,Exception):
    """ an object to keep track of errors occuring to the live object
    """
    def __new__(cls,*args, **kwargs):
               
        instance = Exception.__new__(cls)
        return instance

    def __init__(self,message):
        """create the error exception

        :param str message: message of the error 
        """
        self.category = "LiveObject"
        self.error_type = "value"
        self.message = f"{self.category} {self.error_type} error: {message}"
       
        raise ValueError(message)

    
    def __repr__(self):
        return f"{self.category}<{id(self)}>  with message: {self.message} , error type: {self.error_type}"

