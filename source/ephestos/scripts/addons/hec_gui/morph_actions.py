import bpy
import bmesh
from ephpy import pylivecoding, hpy


def world_reset(morph):
    morph.world.terminate()
    # morph.world.generate_morph_catalog()


class TopViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_axis(type="TOP")


class FrontViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_axis(type="FRONT")


class RightViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_axis(type="RIGHT")


class BottomViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_axis(type="BOTTOM")


class BackViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_axis(type="BACK")


class LeftViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_axis(type="LEFT")


class CameraViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.view3d.view_camera()


class QuadViewButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.screen.region_quadview()


class ObjectModeButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        if morph.world.context.mode == "EDIT_MESH":
            bpy.ops.object.editmode_toggle()
            morph.parent.expand()
            morph.is_enabled = True
            morph.world.get_morph_named("edit_tab_container").colapse()
            morph.world.get_morph_named(
                "edit_tab_container"
            ).main_morph.is_enabled = False


class EditModeButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        if morph.world.context.mode == "OBJECT":
            bpy.ops.object.editmode_toggle()
            morph.parent.expand()
            morph.is_enabled = True
            morph.world.get_morph_named("object_tab_container").colapse()
            morph.world.get_morph_named(
                "object_tab_container"
            ).main_morph.is_enabled = False


class TweakButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.wm.tool_set_by_id(name="builtin.select")


class SelectBoxButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.wm.tool_set_by_id(name="builtin.select_box")


class TranslateButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.transform.translate("INVOKE_REGION_PREVIEW")


class RotateButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.transform.rotate("INVOKE_REGION_PREVIEW")


class ScaleButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.transform.resize("INVOKE_REGION_PREVIEW")


class DeleteObjectButtonAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.object.delete()


class AddMeshCubeAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_cube_add()


class AddMeshPlaneAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_plane_add()


class AddMeshUvSphereAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_uv_sphere_add()


class AddMeshIcoSphereAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_ico_sphere_add()


class AddMeshCircleAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_circle_add()


class AddMeshTorusAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_torus_add()


class AddMeshCylinderAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_cylinder_add()


class AddMeshConeAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_cone_add()


class AddMeshGridAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_grid_add()


class AddMeshMonkeyAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.mesh.primitive_monkey_add()


class AddBezierCurveAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.curve.primitive_bezier_curve_add()


class AddBezierCircleAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.curve.primitive_bezier_circle_add()


class AddCurveNurbsCurveAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.curve.primitive_nurbs_curve_add()


class AddCurveNurbsCircleAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.curve.primitive_nurbs_circle_add()


class AddCurveNurbsPathAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        world_reset(morph)
        bpy.ops.curve.primitive_nurbs_path_add()


class AddModifierSubsurfaceAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        #morph.world.terminate()
        if 'subsurface' in bpy.context.object.modifiers.keys():
            levels = bpy.context.object.modifiers["subsurface"].levels
            if levels < 6:
                bpy.context.object.modifiers["subsurface"].levels = levels + 1
            else:
                bpy.context.object.modifiers["subsurface"].levels = 0
        else:
            bpy.context.object.modifiers.new("subsurface", "SUBSURF")
            bpy.context.object.modifiers["subsurface"].levels = 1

class AddModifierMirrorAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        #morph.world.terminate()
        if 'mirror' in bpy.context.object.modifiers.keys():
            pass
    
        else:
            bpy.context.object.modifiers.new("mirror", "MIRROR")

class AddModifierDecimateAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        #morph.world.terminate()
        if 'decimate' in bpy.context.object.modifiers.keys():
            pass
    
        else:
            bpy.context.object.modifiers.new("mirror", "DECIMATE")            

class EditModeDeleteAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.mesh.delete(type=morph.world.modal_operator.__class__.edit_mode_selection_mode)
        

class EditModeDisolveAction(pylivecoding.LiveObject):
    def on_left_click(self, morph):
        bpy.ops.mesh.dissolve_mode()
        
        
