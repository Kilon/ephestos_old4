"""/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */
"""
# bpy.data.window_managers[0].windows[0].screen.areas[3].spaces[0].region_3d.view_rotation[2]
from ephpy import pylivecoding
from ephpy import hpy
from ephpy.hpy.layout_engine import *
import cProfile, io, pstats, logging, datetime, os
import bpy, pdb
from . import morph_actions


class GUI(pylivecoding.LiveObject):
    def __init__(self, modal_op, context, event, name="gui"):
        self.central_panel = CentralPanelGUI(
            modal_op, context, event, name="cetral_panel_gui"
        )


class CentralPanelGUI(pylivecoding.LiveObject):
    def __init__(self, modal_op, context, event, profile=False, name=""):

        HEC_OT_main_class = modal_op.__class__

        if HEC_OT_main_class.dev_mode and modal_op.addon_preferences.dev_python_profile:
            modal_op.pr = cProfile.Profile()
            modal_op.pr.enable()
        self.layout_engine = None
        self.layout_engine = LayoutEngine(addon_preferences=modal_op.addon_preferences)
        self.layout_engine.morph_actions = morph_actions
        self.layout_engine.load_default_layout_file()
        self.generated_world = self.layout_engine.generate_morphs()

        HEC_OT_main_class.world = self.layout_engine.generated_world
        HEC_OT_main_class.world.modal_operator = modal_op
        HEC_OT_main_class.world.refresh_layout_on_event = False
        HEC_OT_main_class.world.position = [
            event.mouse_region_x + context.region.x,
            event.mouse_region_y + context.region.y,
        ]

        if modal_op.addon_preferences.dev_mode:
            HEC_OT_main_class.log_debug = self.initialise_logging("_hec_gui")

        self.world = HEC_OT_main_class.world
        self.world.scale = 1
        object_tab_container = self.world.get_morph_named("object_tab_container")
        edit_tab_container = self.world.get_morph_named("edit_tab_container")

        if context.mode == "OBJECT":
            edit_tab_container.colapse()
            object_tab_container.children[0].is_enabled = True

        elif context.mode == "EDIT_MESH":
            object_tab_container.colapse()
            edit_tab_container.children[0].is_enabled = True

        HEC_OT_main_class.world.on_event(event=event, context=context)

        if HEC_OT_main_class.dev_mode and modal_op.addon_preferences.dev_python_profile:
            modal_op.pr.disable()
            s = io.StringIO()
            sortby = "cumulative"
            modal_op.ps = pstats.Stats(modal_op.pr, stream=s).sort_stats(sortby)
            modal_op.ps.print_stats()
            modal_op.log_debug.debug(
                "=================================================="
            )
            modal_op.log_debug.debug(
                "|         START PROFILE INITIALISATION           |"
            )
            modal_op.log_debug.debug(
                "=================================================="
            )
            modal_op.log_debug.debug(
                f"profile initialisation of world:  {s.getvalue()}"
            )
            modal_op.log_debug.debug(
                "=================================================="
            )
            modal_op.log_debug.debug(
                "|         END PROFILE INITIALISATION             |"
            )
            modal_op.log_debug.debug(
                "=================================================="
            )
            modal_op.pr = None

    def initialise_logging(self, name, level=logging.DEBUG):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        log_setup = logging.getLogger(name)
        formatter = logging.Formatter(
            "%(asctime)s %(levelname)s: %(message)s", datefmt="%d/%m/%Y %H:%M:%S "
        )
        fileHandler = logging.FileHandler(dir_path + f"/{name}.log", mode="a")
        fileHandler.setFormatter(formatter)
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)
        log_setup.setLevel(level)
        log_setup.addHandler(fileHandler)
        log_setup.addHandler(streamHandler)
        log = log_setup

        log.debug(
            ""
            "\n"
            "\n"
            "\n"
            "***********************************\n"
            "*           NEW SESSION           *\n"
            "***********************************\n"
        )
        now = datetime.datetime.now()
        log.debug(
            "\n LOG DATE : {}/{}/{} {}:{}:{}\n".format(
                now.day, now.month, now.year, now.hour, now.minute, now.second
            )
        )

        log.debug("dir_path : {}".format(dir_path))

        return log
