/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Developer: Dimitris Chloupis
 * 
 * The Original Code is Copyright (C) 2018 by Dimitris Chloupis
 * All rights reserved.
 * ***** END GPL LICENSE BLOCK *****
 */

#ifndef __EPH__HECATE_CORE_H__
#define __EPH__HECATE_CORE_H__

#include "WM_types.h"
#include "BKE_context.h"
#include "DNA_windowmanager_types.h"
#include "UI_interface.h"
#include "IMB_imbuf_types.h"
#include "WM_api.h"
#include "wm_event_system.h"
#include "IMB_imbuf.h"
#include "BKE_appdir.h"
#include "UI_resources.h"
#include "interface_intern.h"

#include "aura_core.h"
typedef enum{
HEC_NO_ERROR ,
HEC_ERROR_MORPH_MALLOC_FAILED , 
HEC_ERROR_TEXTURE_MALLOC_FAILED ,
}hec_errors; 

/* struct for describing the button's/icon properties */
typedef struct hec_icon_data
{
	char *filename;
	char *operator_name;
}hec_icon_data;

/* aci_arg is used by the request function to pass arguments to internal functions
of a component that is loaded, it represesnt a single agrument of various types.
Please not only one type can be used at a time and that dynamic typing is not 
supported for performance reasons */
struct aci_arg
{
	char *type;
	char *string_value;
	long long_value;
	double double_value;
	bContext *context;
	wmOperator *operator;
	ARegion *region;
	wmEvent *event;
	uiBlock *block_pointer;
	
};

/* represents a morph. Generation and manipulation of morphs , together with
handling their events takes place at python side in python module 
ephy.hpy.core , so this struct is used only to keep track of morph properties
purely for drawing purposes. For more info see the python module */

typedef struct Morph Morph;
typedef struct TextureMorph TextureMorph;
typedef struct WorldMorph WorldMorph;



struct Morph
{
	int x;
	int y;
	long width;
	long height;
	char *type;
	int id;
	char *operator_name;
	char *text;
	TextureMorph *texture;
	uiBut *but;
	Morph *next;
	Morph *previous;
	WorldMorph *world;
};

struct TextureMorph
{
	ImBuf *image_buffer;
	ImBuf *image_buffer_backup; /* because blender releases the image in every refresh */
	char *name;
	TextureMorph *next;
	TextureMorph *previous;
};


struct WorldMorph
{	
	ARegion *region;
	uiBlock *block;
	TextureMorph *first_texture;
	TextureMorph *last_texture; 
	Morph *first_morph;
	Morph *last_morph;
	int x;
	int y;
	int width;
	int height;
	int py_region_x;
	int py_region_y;
	int py_region_width;
	int py_region_height;
	int py_mouse_x;
	int py_mouse_y;
	int offset_x;
	int offset_y;
	int is_hidden;
	uiBut *logb; //text label used for logging
	bool should_refresh;
	wmEventHandler_UI *window_modal_handler;
};


WorldMorph* eph_hecate_get_world(void);
void eph_hecate_free_imbufs(void);
void eph_hecate_free_morphs(void);

/* use this struct to keep track of pointers to blender functions exported to components */
struct aci_imported_functions
{
	const char *(*bl_BKE_appdir_folder_id)(const int folder_id, const char *subfolder);
	int(*bl_snprintf)(char *str, size_t size, const char *format, ...);
	ImBuf *(*bl_IMB_loadiffname)(const char *filepath, int flags, char colorspace[IM_MAX_SPACE]);
	ImBuf *(*bl_IMB_dupImBuf)(const ImBuf *ibuf1);
	uiBut *(*bl_uiDefIconButO)(uiBlock *block, int type, const char *opname, int opcontext, int icon, int x, int y, short width, short height, const char *tip);
	uiBut *(*bl_uiDefBut)(uiBlock *block, int type, int retval, const char *str, int x, int y, short width, short height, void *poin, float min, float max, float a1, float a2, const char *tip);
	uiBut *(*bl_uiDefIconBut)(uiBlock *block, int type, int retval, int icon, int x, int y, short width, short height, void *poin, float min, float max, float a1, float a2,  const char *tip);
	void(*bl_BLF_batch_draw_flush)(void);
	void(*bl_BLF_size)(int fontid, int size, int dpi);
	int(*bl_BLF_default)(void);
	void(*bl_BLF_position)(int fontid, float x, float y, float z);
	void(*bl_BLF_draw)(int fontid, const char *str, size_t len);
	void(*bl_UI_but_flag_enable)(uiBut *but, int flag);
	void(*bl_UI_block_bounds_set_popup)(uiBlock *block, int addval, const int bounds_offset[2]);
	void (*bl_UI_block_translate)(uiBlock *block, int x, int y);
	void (*ED_region_tag_redraw)(ARegion *ar);
	WorldMorph *(*eph_hecate_get_world)();
	void* (*eph_malloc)(size_t mems);
	void (*eph_free)(void * mpointer);
	
};
#endif /* __EPH_HECATE_CORE_H__ */


