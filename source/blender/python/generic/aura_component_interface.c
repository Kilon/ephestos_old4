#include <Python.h>

#include "BLI_utildefines.h"
#include "BLI_string.h"

#include "py_capi_utils.h"

#include "python_utildefines.h"
#include "BLI_dynlib.h"

#include <stdio.h>
#include <string.h>
#include "aura_component_interface.h"
#include "ephestos.h"

PyDoc_STRVAR(aci_py_request_doc,
".. function:: request(name,args)\n"
"\n"
"   Make a request to Aura to execute a function.\n"
"\n"
"   :arg name: function name.\n"
"   :type name: string.\n"
"   :arg args: function arguments.\n"
"   :type args: list.\n"
);


static PyObject *aci_py_request(PyObject *UNUSED(self), PyObject *args, PyObject *kw)
{
	PyObject *aci_args = NULL;
	char *aci_request_name;
	static const char *_keywords[] = {"function_name","args", NULL};
	static _PyArg_Parser _parser = {"sO:request", _keywords, 0};
	if (!_PyArg_ParseTupleAndKeywordsFast(
	            args, kw, &_parser,
	            &aci_request_name,&aci_args))
	{
		return NULL;
	}

	if (PyList_Check(aci_args)) {
		/*iterate the list and convert the arguments to c types */
		Py_ssize_t args_count = PyList_Size(aci_args);
		struct aci_arg *aci_arg_list;
		if(args_count == 0)
		{
			aci_arg_list = NULL;
		}
		else
		{
			aci_arg_list=(struct aci_arg*)malloc(sizeof(struct aci_arg)*args_count);
		}
		
		for(int x=0; x<args_count; x++)
		{
			PyObject *args_entry =  PyList_GetItem(aci_args,x);
			if(PyUnicode_Check(args_entry))
			{
				aci_arg_list[x].type = "string";
				aci_arg_list[x].string_value = PyUnicode_AsUTF8(args_entry);
			}
			else if(PyLong_Check(args_entry))
			{
				aci_arg_list[x].type = "long";
				aci_arg_list[x].long_value=PyLong_AsLong(args_entry);
			}
			else if(PyFloat_Check(args_entry))
			{
				aci_arg_list[x].type = "double";
				aci_arg_list[x].double_value = PyFloat_AsDouble(args_entry);
			}
		}
		char error_message[80]="";

		aci_request_if(aci_request_name,args_count,aci_arg_list,error_message);
		if(strcmp("done",error_message )!=0)
		{
			PyErr_SetString(PyExc_TypeError, error_message);
			return NULL;
		}
		if(aci_arg_list != NULL)
		{
			free(aci_arg_list);
		}
	}
	else 
	{
		PyErr_SetString(PyExc_TypeError, "the argument should be a python list");
		return NULL;
	}

	Py_RETURN_NONE;
};

PyDoc_STRVAR(aci_py_unload_doc,
".. function:: request(name,args)\n"
"\n"
"   Make a request to aura to execute a function.\n"
"\n"
"   :arg name: function name.\n"
"   :type name: string.\n"
"   :arg args: function arguments.\n"
"   :type args: list.\n"
);

static PyObject *aci_py_unload(PyObject *UNUSED(self), PyObject *args, PyObject *kw)
{
	
	char *name;
	static const char *_keywords[] = {"name", NULL};
	static _PyArg_Parser _parser = {"s:request", _keywords, 0};
	if (!_PyArg_ParseTupleAndKeywordsFast(
	            args, kw, &_parser,
	            &name))
	{
		return NULL;
	}
	
	
	aci_unload(name);

	Py_RETURN_NONE;
}

PyDoc_STRVAR(aci_py_load_doc,
".. function:: request(name,args)\n"
"\n"
"   Make a request to aura to execute a function.\n"
"\n"
"   :arg name: function name.\n"
"   :type name: string.\n"
"   :arg args: function arguments.\n"
"   :type args: list.\n"
);

static PyObject *aci_py_load(PyObject *UNUSED(self), PyObject *args, PyObject *kw)
{
	
	char *name;
	char *relative_path;
	static const char *_keywords[] = {"name","relative_path",NULL};
	static _PyArg_Parser _parser = {"ss:request", _keywords, 0};
	if (!_PyArg_ParseTupleAndKeywordsFast(
	            args, kw, &_parser,
	            &name, &relative_path))
	{
		return NULL;
	}
	aci_load(name,relative_path);

	Py_RETURN_NONE;
}
static PyMethodDef aura_component_interface_methods[] = {
	{"request", (PyCFunction) aci_py_request, METH_VARARGS | METH_KEYWORDS, aci_py_request_doc},
	{"load", (PyCFunction) aci_py_load, METH_VARARGS | METH_KEYWORDS, aci_py_load_doc},
	{"unload", (PyCFunction) aci_py_unload, METH_VARARGS | METH_KEYWORDS, aci_py_unload_doc},
	{NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(aura_component_interface_module_doc,
"This module provides access to Blender's image manipulation API."
);
static struct PyModuleDef aura_component_interface_module_def = {
	PyModuleDef_HEAD_INIT,
	"aci",  /* m_name */
	aura_component_interface_module_doc,  /* m_doc */
	0,  /* m_size */
	aura_component_interface_methods,  /* m_methods */
	NULL,  /* m_reload */
	NULL,  /* m_traverse */
	NULL,  /* m_clear */
	NULL,  /* m_free */
};

PyObject *BPyInit_aura_component_interface_module(void)
{
	PyObject *submodule;

	submodule = PyModule_Create(&aura_component_interface_module_def);

	//PyType_Ready(&Py_ImBuf_Type);

	return submodule;
};
